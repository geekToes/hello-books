(function() {
    window.addEventListener('load', function(e) {
        let fav_book_listing = document.getElementsByClassName('fav-book-listing');
        let profile_details = document.getElementsByClassName('profile_details');
        let borrowed_books = document.getElementsByClassName('borrowed_books');
        let sidebar_options = document.querySelector('.side-bar');
        let option_list = sidebar_options.querySelectorAll('p');
        option_list.forEach((element, index) => {
            element.addEventListener('click', (e) => {
                if (!e.target.classList.contains('active')) {
                    e.target.classList.add('active');
                    if (index == 0) {
                        fav_book_listing[0].classList.remove('hide');
                        option_list[1].classList.remove('active');
                        // option_list[2].classList.remove('active');
                        borrowed_books[0].classList.add('hide');
                    } else {
                        option_list[0].classList.remove('active');
                        // option_list[0].classList.remove('active');
                        fav_book_listing[0].classList.add('hide');
                        borrowed_books[0].classList.remove('hide');
                    }
                }
            })
        }, this);
        console.log(option_list);
    })
})()